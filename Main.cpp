/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

// Local Headers
#include "Global.hpp"
#include "DesQDock.hpp"
#include "Common.hpp"

#include <desq/DesQUtils.hpp>
#include <desqwl/DesQWlGlobal.hpp>
#include <desqwl/DesQRegistry.hpp>
#include <desqwl/DesQLayerShell.hpp>
#include <desqwl/DesQHotSpot.hpp>
#include <desqwl/DesQWindowManager.hpp>

QDBusInterface *winMgr;
DesQ::Settings *dockSett;
DockSettings *dSett;
QFontMetrics* fm;

DesQWindowManager *desqWinMgr;
DesQWaylandRegistry *desqReg;

void show( DesQDock *dock ) {

	if ( dock->isVisible() )
		return;

    /* Show the dock */
	dock->show();

	DesQLayerSurface *cls = new DesQLayerSurface( dock->windowHandle() );
	cls->setup( desqReg->layerShell(), DesQLayerShell::Overlay );

    switch( dSett->position ) {
        case 1: {// Left
            cls->setAnchors( DesQLayerSurface::Left );
            break;
        }
        case 2: {// Top
            cls->setAnchors( DesQLayerSurface::Top );
            break;
        }
        case 4: {// Right
            cls->setAnchors( DesQLayerSurface::Right );
            break;
        }
        case 8: {// Bottom
            cls->setAnchors( DesQLayerSurface::Bottom );
            break;
        }

        default: {
            // Will not come here
            break;
        }
    }

	cls->setSurfaceSize( dock->size() );
	cls->setMargins( QMargins( 0, 0, 0, 0 ) );
	cls->setExclusiveZone( -1 );
	cls->setKeyboardInteractivity( DesQLayerSurface::NoFocus );
	cls->apply();
};

int main( int argc, char **argv ) {

	qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

	qInstallMessageHandler( Logger );
	QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

	QApplication app( argc, argv );

    /* DesQ Settings for Dock */
	dockSett = new DesQ::Settings( "Dock" );
    QObject::connect( dockSett, &DesQ::Settings::settingChanged, reloadSettings );

    /* Internal pointer */
	dSett = new DockSettings{};
    loadSettings();

    /* DBus based Window Manager */
	winMgr = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor", QDBusConnection::sessionBus() );

    /* Font Metrics object for label widths (needed?) */
	fm = new QFontMetrics( qApp->font() );

    /* Wayland display */
	wl_display *display = DesQWayland::getWlDisplay();
	if ( not display ) {
		qDebug() << "Unable to acquire wl_display from the compositor.";
		qDebug() << "Your experience will be severly limited.";
	}

    /* DesQ Wayland registry */
	desqReg = new DesQWaylandRegistry( display );
	QObject::connect(
		desqReg, &DesQWaylandRegistry::errorOccured, [=]( DesQWaylandRegistry::ErrorType et ) {
			qDebug() << "Error caused on registry" << et;
			qDebug() << "Valiantly trying to continue...";
		}
	);
	desqReg->initialize();

    /* WLR Window Manager */
	desqWinMgr = desqReg->windowManager();

    /* Our dock */
	DesQDock *dock = new DesQDock();

    /* Hotspot to show the dock */
	DesQHotSpot *dockHS = new DesQHotSpot( qApp->primaryScreen(), desqReg->wayfireShell(), dock );
	switch( dSett->position ) {
        case 1: {// Left
            dockHS->createHotspot( DesQHotSpot::Left, 1, 250 );
            break;
        }
        case 2: {// Top
            dockHS->createHotspot( DesQHotSpot::Top, 1, 250 );
            break;
        }
        case 4: {// Right
            dockHS->createHotspot( DesQHotSpot::Right, 1, 250 );
            break;
        }
        case 8: {// Bottom
            dockHS->createHotspot( DesQHotSpot::Bottom, 1, 250 );
            break;
        }

        default: {
            // Will not come here
            break;
        }
    }
	QObject::connect( dockHS, &DesQHotSpot::enteredHotspot, [=]() { show( dock ); } );

    /* Show the dock as layer surface */
	show( dock );

	return app.exec();
};

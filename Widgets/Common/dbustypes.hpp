#pragma once

#include <QtCore>
#include <QtDBus>

typedef QList<uint> QUIntList;

typedef struct WorkSpace_t {
    int rows = 0;
    int columns = 0;
} WorkSpace;

typedef QList<WorkSpace> WorkSpaces;
typedef QMap<uint, WorkSpace> WorkSpaceMap;

Q_DECLARE_METATYPE( QUIntList );
Q_DECLARE_METATYPE( WorkSpace );
Q_DECLARE_METATYPE( WorkSpaces );
Q_DECLARE_METATYPE( WorkSpaceMap );

QDBusArgument &operator<<(QDBusArgument &argument, const WorkSpace &ws);
const QDBusArgument &operator>>(const QDBusArgument &argument, WorkSpace &ws);

bool operator==( const WorkSpace &lhs, const WorkSpace &rhs );

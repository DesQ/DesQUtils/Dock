#include "dbustypes.hpp"

// Marshall the WorkSpace data into a D-Bus argument
QDBusArgument &operator<<(QDBusArgument &argument, const WorkSpace &ws) {
    argument.beginStructure();
    argument << ws.rows;
    argument << ws.columns;
    argument.endStructure();
    return argument;
};

// Retrieve the WorkSpace data from the D-Bus argument
const QDBusArgument &operator>>(const QDBusArgument &argument, WorkSpace &ws) {
    argument.beginStructure();
    argument >> ws.rows;
    argument >> ws.columns;
    argument.endStructure();
    return argument;
};

bool operator==( const WorkSpace &lhs, const WorkSpace &rhs ) {

	return ( ( lhs.rows == rhs.rows ) && ( lhs.columns == rhs.columns ) );
};

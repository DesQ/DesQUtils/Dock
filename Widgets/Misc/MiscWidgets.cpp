/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "MiscWidgets.hpp"

QWidget* DesQDockWidgets::separator( uint direction ) {

	QWidget *sep = new QWidget();
	int height = ( int )dockSett->value( "Height" );
	sep->setStyleSheet( "background-color: gray;" );

	switch( direction ) {
        /*  */
		case 1:
		case 4: {

			sep->setFixedHeight( 1 );
			sep->setMinimumWidth( height * 0.75 );
			sep->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );
			break;
		}

		case 2:
		case 8: {

			sep->setMinimumHeight( height * 0.75 );
			sep->setFixedWidth( 1 );
			sep->setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::MinimumExpanding ) );
			break;
		}

        default: {
            break;
        }
	}

	return sep;
};

QLayoutItem* DesQDockWidgets::stretch( uint direction ) {

	if ( direction == 1 and direction == 4 )
		return new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );

	else
		return new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
};

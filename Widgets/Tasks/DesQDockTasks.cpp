/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Global.hpp"
#include "DesQDockTasks.hpp"

#include <filesystem>
#include <desqui/DesQLayout.hpp>
#include <desq/DesQUtils.hpp>

#include <desqwl/Registry.hpp>
#include <desqwl/WindowManager.hpp>

static inline QIcon getIconForAppId( QString mAppId ) {

    if ( mAppId == "Unknown" )
        return QIcon::fromTheme( "application-x-executable" );

    QStringList appDirs = {
        QDir::home().filePath( ".local/share/applications/" ),
        "/usr/local/share/applications/",
        "/usr/share/applications/",
    };

    QString iconName;
    bool found = false;
    for( QString path: appDirs ) {
        if ( DesQ::Utils::exists( path + mAppId + ".desktop" ) ) {
            QSettings desktop( path + mAppId + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
            if ( not iconName.isEmpty() ) {
                found = true;
                break;
            }
        }

        else if ( DesQ::Utils::exists( path + mAppId.toLower() + ".desktop" ) ) {
            QSettings desktop( path + mAppId.toLower() + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
            if ( not iconName.isEmpty() ) {
                found = true;
                break;
            }
        }
    }

    if ( not found ) {
        /* Check all desktop files for */
        for( QString path: appDirs ) {
            QStringList desktops = QDir( path ).entryList( { "*.desktop" } );
            for ( QString dskf: desktops ) {
                QSettings desktop( path + dskf, QSettings::IniFormat );

                QString exec = desktop.value( "Desktop Entry/Exec", "abcd1234/-" ).toString();
                QString name = desktop.value( "Desktop Entry/Name", "abcd1234/-" ).toString();
                QString cls = desktop.value( "Desktop Entry/StartupWMClass", "abcd1234/-" ).toString();

                QString execPath( std::filesystem::path( exec.toStdString() ).filename().c_str() );
                if ( mAppId.compare( execPath, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }

                else if ( mAppId.compare( name, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }

                else if ( mAppId.compare( cls, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }
            }
        }
    }

    return QIcon::fromTheme( iconName, QIcon::fromTheme( "application-x-executable" ) );
};

DesQTask::DesQTask( DesQWindowHandle *hndl ) : QObject() {

    mHandle = hndl;

    connect(
        hndl, &DesQWindowHandle::appIdUpdated, [=]() {
            mAppId = hndl->appTitle();
            mTitle = hndl->appTitle();

            mMaximized = hndl->windowStates() & DesQWindowHandle::Maximized;
            mMinimized = hndl->windowStates() & DesQWindowHandle::Minimized;
            mActivated = hndl->windowStates() & DesQWindowHandle::Activated;

            if ( mActivated )
                mAttention = false;

            emit updateTask();
        }
    );

    connect(
        hndl, &DesQWindowHandle::handleUpdated, [=]() {
            mAppId = hndl->appTitle();
            mTitle = hndl->appTitle();

            mMaximized = hndl->windowStates() & DesQWindowHandle::Maximized;
            mMinimized = hndl->windowStates() & DesQWindowHandle::Minimized;
            mActivated = hndl->windowStates() & DesQWindowHandle::Activated;

            if ( mActivated )
                mAttention = false;

            emit updateTask();
        }
    );

    connect(
        hndl, &DesQWindowHandle::handleClosed, [=]() {
            deleteLater();
            emit taskClosed();
        }
    );

    mAppId = hndl->appID();
    mTitle = hndl->appTitle();

    mMaximized = hndl->windowStates() & DesQWindowHandle::Maximized;
    mMinimized = hndl->windowStates() & DesQWindowHandle::Minimized;
    mActivated = hndl->windowStates() & DesQWindowHandle::Activated;
};

QString DesQTask::appId() {

    return mAppId;
};

QString DesQTask::title() {

    return mTitle;
};

bool DesQTask::isActivated() {

    return mActivated;
};

void DesQTask::activate() {

    if ( mActivated )
        return;

    mHandle->activate( desqReg->wlSeat() );
};

bool DesQTask::hasAttention() {

    return mAttention;
};

void DesQTask::setAttention( bool yes ) {

    mAttention = yes;
};

DesQTaskGroup::DesQTaskGroup( QWidget *parent, QString appid ) : QWidget( parent ) {

    mAppId = appid;

    /* Dummy for getting icons */
    mIcon = getIconForAppId( appid );

    /* Widget & Icon sizes */
    int wh = ( int )dockSett->value( "Height" ) - 4;
    mSize = QSize( wh, wh );
    setFixedSize( mSize );
    mIconSize = mSize - QSize( 4, 4 );
};

void DesQTaskGroup::addTask( DesQTask *task ) {

    if ( groupTasks.contains( task ) )
        return;

    groupTasks << task;

    if ( task->isActivated() )
        mHasFocus = true;

    if ( task->hasAttention() )
        mHasAttention = true;

    connect(
        task, &DesQTask::updateTask, [=]() {
            if ( task->isActivated() )
                mHasFocus = true;

            if ( task->hasAttention() )
                mHasAttention = true;

            repaint();
        }
    );

    connect(
        task, &DesQTask::taskClosed, [=]() {
            removeTask( task );
        }
    );

    repaint();
};

void DesQTaskGroup::removeTask( DesQTask *task ) {

    if ( not groupTasks.contains( task ) )
        return;

    groupTasks.removeAll( task );

    if ( not groupTasks.count() ) {
        deleteLater();
        return;
    }

    repaint();
};

int DesQTaskGroup::taskCount() {

    return groupTasks.count();
};

void DesQTaskGroup::setFocus( DesQTask *task ) {

    mHasFocus = ( groupTasks.contains( task ) ? true : false );

    repaint();
};

void DesQTaskGroup::setAttention( DesQTask *task, bool yes ) {

    if ( not groupTasks.contains( task ) )
        return;

    task->setAttention( yes );

    mHasAttention = false;
    for( DesQTask *task: groupTasks )
        mHasAttention |= task->hasAttention();

    repaint();
};

void DesQTaskGroup::mousePressEvent( QMouseEvent *mEvent ) {

    if ( mEvent->button() == Qt::LeftButton )
        mPressed = true;

    mEvent->accept();
};

void DesQTaskGroup::mouseReleaseEvent( QMouseEvent *mEvent ) {

    if ( mEvent->button() == Qt::LeftButton and mPressed ) {
        groupTasks.at( 0 )->activate();
    }

    mEvent->accept();
};


void DesQTaskGroup::paintEvent( QPaintEvent *pEvent ) {

    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );

    painter.drawPixmap( QRect( QPoint( 2, 2 ), mIconSize ), mIcon.pixmap( mIconSize ) );

    painter.save();
    /* Attention takes precedence */
    if ( mHasAttention )
        painter.setPen( QPen( QColor( "#B95000" ), 1.5 ) );

    else if ( mHasFocus )
        painter.setPen( QPen( palette().color( QPalette::Highlight ), 2.0 ) );

    else
        painter.setPen( Qt::NoPen );

    painter.drawLine( QPoint( 1, 1 ), QPoint( width(), 1 ) );
    painter.restore();

    if ( groupTasks.count() > 1 ) {
        // Bottom-right corner badge shows the number of windows
        QRect badge( mSize.width() - 16, mSize.height() - 16, 16, 16 );
        painter.save();
        painter.setPen( Qt::NoPen );
        painter.setBrush( palette().color( QPalette::Highlight ) );
        painter.drawEllipse( badge );
        painter.restore();

        painter.save();
        painter.setPen( palette().color( QPalette::HighlightedText ) );
        painter.setBrush( Qt::NoBrush );
        painter.drawText( badge, Qt::AlignCenter, QString::number( groupTasks.count() ) );
        painter.restore();
    }

    painter.end();
};

DesQDockTasks::DesQDockTasks() : QWidget() {

    connect( desqWinMgr, &DesQWindowManager::newTopLevelHandle, this, &DesQDockTasks::addView );
    setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );

    tasksLyt = new QHBoxLayout();
    tasksLyt->setContentsMargins( QMargins() );
    tasksLyt->setSpacing( 2 );
    setLayout( tasksLyt );

    for( DesQWindowHandle *hndl: desqWinMgr->windowHandles() )
        addView( hndl );
};

QSize DesQDockTasks::sizeHint() const {

    return QSize( tasksGroup.count() * 32 + ( tasksGroup.count() - 1 ) * 2, 36 );
};

QSize DesQDockTasks::minimumSizeHint() const {

    return QSize( tasksGroup.count() * 32 + ( tasksGroup.count() - 1 ) * 2, 36 );
};

void DesQDockTasks::addView( DesQWindowHandle *hndl ) {

    if ( not hndl ) {
        qDebug() << "Bad handle";
        return;
    }

    DesQTask *task = new DesQTask( hndl );
    handleTaskMap[ hndl ] = task;

    if ( not handles.contains( hndl ) )
        handles << hndl;

    QString appid( task->appId() );

    if ( not tasksGroup.contains( appid ) ) {
        tasksGroup[ appid ] = new DesQTaskGroup( this, appid );
        tasksLyt->addWidget( tasksGroup[ appid ] );
    }

    tasksGroup[ appid ]->addTask( task );

    connect(
        hndl, &DesQWindowHandle::appIdUpdated, [=]( QString oldId, QString newId ) {
            regroupView( hndl, oldId, newId );
        }
    );

    /* Just shown, but no focus */
    if ( not task->isActivated() )
        tasksGroup[ appid ]->setAttention( task, true );
};

void DesQDockTasks::regroupView( DesQWindowHandle* hndl, QString oldId, QString newId ) {

    DesQTask *task = handleTaskMap[ hndl ];

    if ( tasksGroup.contains( oldId ) )
        tasksGroup[ oldId ]->removeTask( task );

    if ( not tasksGroup.contains( newId ) ) {
        tasksGroup[ newId ] = new DesQTaskGroup( this, newId );
        tasksLyt->addWidget( tasksGroup[ newId ] );
    }

    tasksGroup[ newId ]->addTask( task );

    setFixedSize( QSize( tasksGroup.count() * 32 + ( tasksGroup.count() - 1 ) * 2, 36 ) );
};

/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "dbustypes.hpp"

#include <QtWidgets>
#include <desqwl/DesQWindowManager.hpp>

class DesQLayout;
class DesQWaylandRegistry;

class DesQTask: public QObject {
	Q_OBJECT;

	public:
		DesQTask( DesQWindowHandle *hndl );

		/* AppID and Title */
		QString appId();
		QString title();

		/* Remember attention state */
		void setAttention( bool );
		bool hasAttention();

		/* Has focus? */
		bool isActivated();
		void activate();

	private:
		DesQWindowHandle *mHandle;

		QString mAppId;
		QString mTitle;

		bool mMaximized = false;
		bool mMinimized = false;
		bool mActivated = false;

		bool mAttention = false;

	Q_SIGNALS:
		void updateTask();
		void taskClosed();
};

typedef QList<DesQTask*> DesQTaskList;

class DesQTaskGroup : public QWidget {
	Q_OBJECT;

	public:
		DesQTaskGroup( QWidget *parent, QString appid );

		/* Tasks added/removed */
		void addTask( DesQTask * );
		void removeTask( DesQTask * );

		/* Task count */
		int taskCount();

		/* One of the members of this group has focus */
		void setFocus( DesQTask * );

		/* One or more of the members of this group seeks attention */
		void setAttention( DesQTask *, bool );

	private:
		bool mHasFocus = false;
		bool mHasAttention = false;
		DesQTaskList groupTasks;

		QString mAppId;
		QIcon mIcon;

		QSize mSize;
		QSize mIconSize;

		bool mPressed = false;

	protected:
		void paintEvent( QPaintEvent *pEvent );

		void mousePressEvent( QMouseEvent *mEvent );
		void mouseReleaseEvent( QMouseEvent *mEvent );
};

class DesQDockTasks : public QWidget {
	Q_OBJECT;

	public:
		DesQDockTasks();

		QSize sizeHint() const;
		QSize minimumSizeHint() const;

	private:
		QHBoxLayout *tasksLyt;

		QWidget *separator;

		QMap<uint, DesQTask> tasks;
		QMap<QString, DesQTaskGroup*> tasksGroup;

		DesQWindowHandleList handles;
		QMap<DesQWindowHandle*, DesQTask*> handleTaskMap;

	public Q_SLOTS:
		void addView( DesQWindowHandle* );
		void regroupView( DesQWindowHandle*, QString, QString );
};

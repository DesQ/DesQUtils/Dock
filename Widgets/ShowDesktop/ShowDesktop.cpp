/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "ShowDesktop.hpp"

ShowDesktop::ShowDesktop( uint direction ) : QWidget() {

    setMouseTracking( true );

    int height = dockSett->value( "Height" );

    /* Panel is on the left or right */
    if ( direction == 1u or direction == 4u )
        setFixedSize( QSize( height - 4, 20 ) );

    else
        setFixedSize( QSize( 20, height - 4 ) );

    mInside = false;
};

void ShowDesktop::enterEvent( QEvent *event ) {

    mInside = true;
    repaint();

    winMgr->call( "ShowDesktop" );

    event->accept();
};

void ShowDesktop::leaveEvent( QEvent *event ) {

    mInside = false;
    repaint();

    winMgr->call( "ShowDesktop" );

    event->accept();
};

void ShowDesktop::paintEvent( QPaintEvent *pEvent ) {

    QPainter painter( this );
    painter.setRenderHints( QPainter::Antialiasing );

    painter.setPen( Qt::NoPen );

    if ( mInside )
        painter.setBrush( QColor( 0x80, 0x80, 0x80, 0x30 ) );

    else
        painter.setBrush( QColor( 0x80, 0x80, 0x80, 0x99 ) );

    painter.drawRoundedRect( QRect( 5, 2, 10, height() - 4 ), 5, 5 );
    painter.end();

    pEvent->accept();
}

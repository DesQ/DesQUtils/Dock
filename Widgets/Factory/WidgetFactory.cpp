/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "WidgetFactory.hpp"
#include "Widgets.hpp"

QWidget* DesQDockWidgetFactory::createWidget( QString name ) {

    int direction = dockSett->value( "Position" );

    if ( name == "DesQStart" ) {
        DesQStart *start = new DesQStart();
        return start;
    }

    else if ( name == "Separator" ) {

        return DesQDockWidgets::separator( direction );
    }

    else if ( name == "Pager" ) {
        DesQDockPager *pager = new DesQDockPager();
        return pager;
    }

    else if ( name == "Tasks" ) {

        DesQDockTasks *tasks = new DesQDockTasks();
        return tasks;
    }

    else if ( name == "Notifications" ) {

        Notifications *info = new Notifications();
        return info;
    }

    else if ( name == "Tray" ) {

        Notifications *tray = new Notifications();
        tray->setIcon( QIcon::fromTheme( "desq" ) );

        return tray;
    }

    else if ( name == "Clock" ) {

        SimpleClock *clock = new SimpleClock();
        return clock;
    }

    else if ( name == "ShowDesktop" ) {

        ShowDesktop *sd = new ShowDesktop( direction );
        return sd;
    }

    return nullptr;
};

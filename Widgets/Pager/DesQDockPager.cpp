/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Global.hpp"
#include "DesQDockPager.hpp"
#include "dbustypes.hpp"

#include <desqui/DesQLayout.hpp>

DesQDockPager::DesQDockPager( QWidget *parent ) : QLabel( parent ) {

    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceMap>( "WorkSpaceMap" );
    qDBusRegisterMetaType<WorkSpaceMap>();

	QDBusConnection::sessionBus().connect(
		"org.DesQ.Wayfire",
		"/org/DesQ/Wayfire",
		"wayland.compositor",
		"OutputWorkspaceChanged",
		"uii",
		this,
		SLOT( highlightWorkspace( uint, int, int ) )
	);

	QDBusReply<uint> output = winMgr->call( "QueryActiveOutput" );

    int x = 0, y = 0;
    if ( output.isValid() ) {
    	QDBusReply<WorkSpace> ws = winMgr->call( "QueryOutputWorkspace", output.value() );
    	x = ( ws.isValid() ? ws.value().columns + 1 : 0 );
    	y = ( ws.isValid() ? ws.value().rows + 1    : 0 );
    }

	setAlignment( Qt::AlignCenter );
	setText( QString( "<tt>(%1, %2)</tt>" ).arg( x ).arg( y ) );

    setFixedWidth( fm->maxWidth() * 4 );
};

DesQDockPager::~DesQDockPager() {
};

void DesQDockPager::highlightWorkspace( uint outputid, int ws_horiz, int ws_vert ) {

	Q_UNUSED( outputid );
    setText( QString( "<tt>(%1, %2)</tt>" ).arg( ws_vert + 1 ).arg( ws_horiz + 1 ) );
};

/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "ClockWidget.hpp"

SimpleClock::SimpleClock() {

	timeFmt = "HH:mm:ss";
	dateFmt = "dddd, MMMM dd, yyyy";

	// Text inside the QLabel
	setAlignment( Qt::AlignCenter );

	QDateTime dt = QDateTime::currentDateTime();
	clockFmt = "<center><large><b>%1</b></large><br>%2</center>";
	setText( QString( clockFmt ).arg( dt.toString( timeFmt ) ).arg( dt.toString( dateFmt ) ) );

	QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
	shadow->setOffset( 1, 1 );
	shadow->setColor( palette().color( QPalette::Window ) );
	setGraphicsEffect( shadow );

	timer = new QBasicTimer();
	timer->start( 1000, this );

	setFixedWidth( fm->maxWidth() * 10 );
};

void SimpleClock::setDateFormat( QString newFmt ) {

	dateFmt = newFmt;
};

void SimpleClock::setTimeFormat( QString newFmt ) {

	timeFmt = newFmt;
};

void SimpleClock::timerEvent( QTimerEvent *event ) {

	if ( event->timerId() == timer->timerId() ) {

		QDateTime dt = QDateTime::currentDateTime();
		setText( QString( clockFmt ).arg( dt.toString( timeFmt ) ).arg( dt.toString( dateFmt ) ) );
	}

	QLabel::timerEvent( event );
};

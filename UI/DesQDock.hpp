/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtWidgets>

class DesQLayout;
class DesQDockIsland;

class DesQDock : public QWidget {
	Q_OBJECT;

	public:
		DesQDock();
		~DesQDock();

	public Q_SLOTS:
		void show();
		void hide();

	protected:
		void paintEvent( QPaintEvent *event ) override;

	private:
		void loadSettings();
		void doLayout();

		QMap<QString, DesQDockIsland> islands;

	Q_SIGNALS:
		void newWindow();
};

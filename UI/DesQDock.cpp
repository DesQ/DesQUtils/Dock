/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Global.hpp"
#include "DesQDock.hpp"
#include "DesQDockIsland.hpp"
#include "Widgets.hpp"

#include <desqui/DesQLayout.hpp>
#include <desqwl/DesQRegistry.hpp>

DesQDock::DesQDock() : QWidget() {

	/* No frame, and stay at bottom */
	Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::BypassWindowManagerHint;
	setAttribute( Qt::WA_TranslucentBackground );

	/* Set the windowFlags */
	setWindowFlags( wFlags );

	doLayout();
};

DesQDock::~DesQDock() {

	winMgr->deleteLater();
};

void DesQDock::show() {

	QWidget::show();
	QTimer::singleShot( 5000, this, &DesQDock::hide );
};

void DesQDock::hide() {

	if ( underMouse() ) {
		QTimer::singleShot( 5000, this, &DesQDock::hide );
		return;
	}

	else
		QWidget::hide();
};

void DesQDock::doLayout() {

	QSize scrSize = qApp->primaryScreen()->size();

	/* Vertical Layout */
	if ( dSett->position == 1 or dSett->position == 4 ) {
		setFixedSize( dSett->height + 2 * dSett->margins, scrSize.height() );

		QVBoxLayout *lyt = new QVBoxLayout();
		lyt->setContentsMargins( dSett->margins, dSett->margins, dSett->margins, dSett->margins );
		lyt->setSpacing( 0 );

		/* ClassicBar */
		if ( dSett->style == 1 ) {
			lyt->addWidget( new DesQDockIsland( "ClassicBar", this ) );
		}

		else {
			int isleCount = dockSett->value( "Islands/Count" );

			for( int c = 1; c <= isleCount; c++ ) {
				lyt->addWidget( new DesQDockIsland( QString( "Island%1" ).arg( c ), this ) );
				if ( c != isleCount )
					lyt->addStretch();
			}
		}

		setLayout( lyt );
	}

	/* Horizontal Layout */
	else {
		setFixedSize( scrSize.width(), dSett->height + 2 * dSett->margins );

		QHBoxLayout *lyt = new QHBoxLayout();
		lyt->setContentsMargins( dSett->margins, dSett->margins, dSett->margins, dSett->margins );
		lyt->setSpacing( 0 );

		/* ClassicBar */
		if ( dSett->style == 1 ) {
			lyt->addWidget( new DesQDockIsland( "ClassicBar", this ) );
		}

		else {
			int isleCount = dockSett->value( "Islands/Count" );

			for( int c = 1; c <= isleCount; c++ ) {
				lyt->addWidget( new DesQDockIsland( QString( "Island%1" ).arg( c ), this ) );
				if ( c != isleCount )
					lyt->addStretch();
			}
		}

		setLayout( lyt );
	}
};

void DesQDock::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( this );
	painter.setRenderHint( QPainter::Antialiasing );

	painter.save();
	painter.setPen( Qt::NoPen );
	painter.setBrush( QColor( 0, 0, 0, 1 ) );
	painter.drawRect( rect() );
	painter.restore();

	painter.end();

	QWidget::paintEvent( pEvent );
};

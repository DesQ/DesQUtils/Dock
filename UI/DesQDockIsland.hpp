/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "Global.hpp"

class DesQLayout;

class DesQDockIsland : public QWidget {
	Q_OBJECT;

	public:
		DesQDockIsland( QString name, QWidget *parent );
		QString name();

		QSize minimumSizeHint() const override;
		QSize sizeHint() const override;

	public Q_SLOTS:
		void show();
		void hide();

	protected:
		void paintEvent( QPaintEvent* ) override;

	private:
		DesQLayout *isleLyt;
		QString isleName;

		/* Settings */
		void doLayout();

		QList<QWidget *> items;

	Q_SIGNALS:
		/*  */
		void dockHidden( QString );
};

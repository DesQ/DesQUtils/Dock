/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*

	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Global.hpp"
#include "DesQDockIsland.hpp"
#include "WidgetFactory.hpp"

#include <desqui/DesQLayout.hpp>
#include <desqwl/DesQRegistry.hpp>

DesQDockIsland::DesQDockIsland( QString name, QWidget *parent ) : QWidget( parent ) {

	isleName = name;

	isleLyt = new DesQLayout();
	isleLyt->setContentsMargins( dSett->margins, dSett->margins, dSett->margins, dSett->margins );
	isleLyt->setHorizontalSpacing( 0 );
	isleLyt->setVerticalSpacing( 0 );
	setLayout( isleLyt );

	doLayout();
};

QString DesQDockIsland::name() {

	return isleName;
};

QSize DesQDockIsland::minimumSizeHint() const {

	QSize size;

	if ( dSett->position == 1 or dSett->position == 4 ) {
		int h = 0;
		for( QWidget *w: items )
			h += w->height();

		size = QSize( dSett->height, h );
	}

	else {
		int w = 0;
		for( QWidget *itm: items )
			w += itm->height();

		size = QSize( w, dSett->height );
	}

	return size;
};

QSize DesQDockIsland::sizeHint() const {

	return minimumSizeHint();
};

void DesQDockIsland::show() {

	QWidget::show();

	if ( dSett->autoHide != 2 )
		QTimer::singleShot( dSett->autoHideTimeOut, this, &DesQDockIsland::hide );
};

void DesQDockIsland::hide() {

	if ( underMouse() ) {
		QTimer::singleShot( dSett->autoHideTimeOut, this, &DesQDockIsland::hide );
		return;
	}

	else {
		QWidget::hide();
		emit dockHidden( isleName );
	}
};

void DesQDockIsland::doLayout() {

	QSize scrSize = qApp->primaryScreen()->size();

	if ( dSett->position == 1 or dSett->position == 4 ) {
		setFixedWidth( dSett->height );
		if ( isleName == "ClassicBar" )
			setFixedHeight( scrSize.height() - dSett->margins * 2 );
		setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Minimum );
	}

	else {
		setFixedHeight( dSett->height );
		setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Fixed );
		if ( isleName == "ClassicBar" )
			setFixedWidth( scrSize.width() - dSett->margins * 2 );
	}

	isleLyt->clear();

	QStringList items;
	if ( isleName == "ClassicBar" )
	 	items = dockSett->value( "ClassicBar/Items" );

	else
		items = dockSett->value( "Islands/" + isleName );

	int size = 0;
	for( QString item: items ) {
		QWidget *w = DesQDockWidgetFactory::createWidget( item );

		if ( w ) {
			size += w->width();
			isleLyt->addWidget( w );
		}
	}

	setFixedWidth( size + 4 );
};

void DesQDockIsland::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( this );
	painter.setRenderHint( QPainter::Antialiasing );

	painter.save();
	painter.setBrush( QColor( 0, 0, 0, 255 * dSett->alpha ) );
	painter.setPen( dSett->borderColor );
	painter.drawRoundedRect( rect(), dSett->corners, dSett->corners );
	painter.restore();

	painter.end();

	QWidget::paintEvent( pEvent );
};

/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

// Local Headers
#include "Common.hpp"

QColor getColor( QString clrStr ) {

    qDebug() << clrStr;

    /* clrStr is actually a color code */
    if ( QString( "1234567890abcdef" ).contains( clrStr.at( 0 ).toLower() ) )
        return QColor( QString( "#%1" ).arg( clrStr ) );

    /* It's a palette reference */
    QPalette pltt( qApp->palette() );
    if ( clrStr == "Window" )
        return pltt.color( QPalette::Window );

    if ( clrStr == "WindowText" )
        return pltt.color( QPalette::WindowText );

    if ( clrStr == "Base" )
        return pltt.color( QPalette::Base );

    if ( clrStr == "AlternateBase" )
        return pltt.color( QPalette::AlternateBase );

    if ( clrStr == "ToolTipBase" )
        return pltt.color( QPalette::ToolTipBase );

    if ( clrStr == "ToolTipText" )
        return pltt.color( QPalette::ToolTipText );

    if ( clrStr == "PlaceholderText" )
        return pltt.color( QPalette::PlaceholderText );

    if ( clrStr == "Text" )
        return pltt.color( QPalette::Text );

    if ( clrStr == "Button" )
        return pltt.color( QPalette::Button );

    if ( clrStr == "ButtonText" )
        return pltt.color( QPalette::ButtonText );

    if ( clrStr == "BrightText" )
        return pltt.color( QPalette::BrightText );

    if ( clrStr == "Highlight" )
        return pltt.color( QPalette::Highlight );

    if ( clrStr == "HighlightedText" )
        return pltt.color( QPalette::HighlightedText );

    if ( clrStr == "Link" )
        return pltt.color( QPalette::Link );

    if ( clrStr == "LinkVisited" )
        return pltt.color( QPalette::LinkVisited );

    /* By default, return no color */
    return QColor( Qt::transparent );
};

void loadSettings() {
	/* General */
    dSett->position = ( int )dockSett->value( "Position" );
    dSett->alignment = ( int )dockSett->value( "Alignment" );
    dSett->style = ( int )dockSett->value( "Style" );
    dSett->corners = ( int )dockSett->value( "Corners" );
    dSett->autoHide = ( int )dockSett->value( "AutoHide" );
    dSett->autoHideTimeOut = ( int )dockSett->value( "AutoHideTimeOut" );
    dSett->margins = ( int )dockSett->value( "Margins" );
    dSett->height = ( int )dockSett->value( "Height" );

    /* Theme */
    dSett->alpha = ( qreal )dockSett->value( "Theme/Alpha" );
    dSett->bgColor = getColor( dockSett->value( "Theme/BackgroundColor" ) );
    dSett->bgColor.setAlphaF( dSett->alpha );

    dSett->borderColor = getColor( dockSett->value( "Theme/BorderColor" ) );
};

void reloadSettings( QString app, QString key, QVariant value ) {

	if ( app != "Dock" )
		return;

    if ( key == "Position" )
        dSett->position = value.toUInt();

    else if ( key == "Alignment" )
        dSett->alignment = value.toUInt();

    else if ( key == "Style" )
        dSett->style = value.toUInt();

    else if ( key == "Corners" )
        dSett->corners = value.toUInt();

    else if ( key == "AutoHide" )
        dSett->autoHide = value.toUInt();

    else if ( key == "AutoHideTimeOut" )
        dSett->autoHideTimeOut = value.toUInt();

    else if ( key == "Margins" )
        dSett->margins = value.toUInt();

    else if ( key == "Height" )
        dSett->height = value.toUInt();

    else if ( key == "Theme/Alpha" )
        dSett->alpha = value.toDouble();

    else if ( key == "Theme/BackgroundColor" )
        dSett->bgColor = value.toString();

    else if ( key == "Theme/BorderColor" )
        dSett->borderColor = value.toString();
};

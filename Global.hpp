/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtDBus>

#include <desq/DesQSettings.hpp>

class DesQWindowManager;
class DesQWaylandRegistry;

extern QDBusInterface *winMgr;
extern DesQ::Settings* dockSett;
extern QFontMetrics* fm;

/* DesQWL */
extern DesQWindowManager *desqWinMgr;
extern DesQWaylandRegistry *desqReg;

/* Initialize with sane settings */
typedef struct _dock_settings_t {
    /* General */
    uint position = 8;
    uint alignment = 4;
    uint style = 2;
    uint corners = 3;
    uint autoHide = 1;
    uint autoHideTimeOut = 5000;
    uint margins = 2;
    uint height = 36;

    /* Theme */
    qreal alpha = 0.72;
    QColor bgColor = QColor( Qt::gray );
    QColor borderColor = QColor( Qt::darkGray );
} DockSettings;

extern DockSettings *dSett;
